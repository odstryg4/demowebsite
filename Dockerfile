FROM php:7.3-apache
ADD ./webapp /var/www/html/
WORKDIR /var/www/html/
# Expose is NOT supported by Heroku
EXPOSE 80 		
# CMD gunicorn --bind 0.0.0.0:$PORT wsgi
