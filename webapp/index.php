<html>
    <head>
        <title>I.C. Demo</title>
        <link rel="stylesheet" href="estilos.css">
    </head>
    <body>
        <h1> Demo de Integración Continua V2</h1>
        <h3>Circuito de integracion continua con: Visual Studio Code -> Bitbucket -> HEROKU</h3>
        <hr>
        <ul>
            <li><a href="contacto.php">Contacto</a></li>
        </ul>

        <p>URL en heroku: <a href="https://phpbitbucket.herokuapp.com/">https://phpbitbucket.herokuapp.com/</a></p>

        <p>URL Bitbucket: <a href="https://bitbucket.org/odstryg4/demowebsite/src/master/">https://bitbucket.org/odstryg4/demowebsite/src/master/</a></p>

        <p><h3>
        <?PHP echo "Esto es PHP -> Fecha y hora actual: " . date("Y-m-d H:i:s"); ?>
        </h3></p>

        <p>V2.0 - DEMO</p>
    </body>
</html>